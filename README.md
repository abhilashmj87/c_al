# README #

The Context-Aware Learner App.

### What is this repository for? ###

* This is a repo for storing the C-AL project.
* v0.1.1

### How do I get set up? ###

The alpha version of the APK for the project is located in the [Downloads](https://bitbucket.org/abhilashmj87/c_al/downloads) folder. You would have to trust unsigned apps and then side-load the APK through a usb port or copy it to a cloud storage system and then download it from there. Or, you would have to download the repository and run the www/index.html file locally (In Firefox).
Change width and height of the browser to 375 * 667 to mimic iPhone resolution. Otherwise, the app would look as if it lost styling.
The App depends on a network connection.

### Where do I log bugs, defects and enhancements? ###

Please use the issue tracker attached to the repo [here](https://bitbucket.org/abhilashmj87/c_al/issues?status=new&status=open) to log any kind of bugs you notice. Please do mention the version number of the app you noticed this bug in.