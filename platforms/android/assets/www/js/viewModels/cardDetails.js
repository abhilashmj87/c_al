define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojtabs', 'ojs/ojconveyorbelt','ojs/ojbutton', 'ojs/ojpopup',
        'ojs/ojmoduleanimations','ojs/ojrouter'
    ], function (oj, ko, $) {

    function accContentModel(moduleParams,navg) {
        var self = this;
        
        self.backToListHandle = function(event, ui){
          console.log(moduleParams);
        //   oj.Router.rootInstance.go('carListDetails');    
         //  moduleParams.content('hello');
           moduleParams.currentModule("carListDetails");
           moduleParams.previousModule("carList");
      }
        
      
        
      self.todayDate = ko.pureComputed(
          function()
          {
              var m_names = new Array("January", "February", "March", 
                                      "April", "May", "June", "July", "August", "September", 
                                      "October", "November", "December");

              var d = new Date();
              var curr_date = d.getDate();
              var curr_month = d.getMonth();
              var curr_year = d.getFullYear();
              var curDate = m_names[curr_month] + " " + curr_date + ", " + curr_year;
              return curDate;
          }
        ); 
        
    }

    return accContentModel ;
});  