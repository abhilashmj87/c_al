define(['knockout', 'ojs/ojknockout', 'ojs/ojcore','jquery'
    ], function (ko) {

    function chatContentModel() {
        var self = this;
    }

    $(document).ready(function()
    {
        var i = 0;
        $('.chat-button').on('click',function(e) {
            var $replies = ["Hi Catherine, How are you? Need help with your service appointment on September 30?","Sure, I am checking the inventory for you.","I see a 2017 eCX available at a different dealership. Let me have the car transferred to the San Francisco dealership for you. Can I  send you an E-Mail confimration later today?","You're welcome. Thank you for being a gold preffered owner with us. You have a nice day."];
            {
                var $text = $('textarea').val();
                $('.disp').html($('.disp').html()+'<div class="catherine-reply">'+$text+'</div>');
                $('textarea').val('');
                $('.hides').css('opacity', 1);
                setTimeout(function() {
                    $('.disp').append('<div class="agent-reply">'+$replies[i]+'</div>');
                    i++;
                    $(".disp").scrollTop($(".disp")[0].scrollHeight);
                    $('.hides').css('opacity', 0);
                }, 5000);
            }
        });
    });

    return chatContentModel ;
});