/**
 * Copyright (c) 2014, 2016, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
"use strict";
var vm1 ;
requirejs.config(
{
  baseUrl: "js",

  // Path mappings for the logical module names
  paths:
  //injector:mainReleasePaths
  {
    "knockout": "libs/knockout/knockout-3.4.0.debug",
    "jquery": "libs/jquery/jquery-2.1.3",
    "jqueryui-amd": "libs/jquery/jqueryui-amd-1.11.4",
    "promise": "libs/es6-promise/promise-1.0.0",
    "hammerjs": "libs/hammer/hammer-2.0.4",
    "ojdnd": "libs/dnd-polyfill/dnd-polyfill-1.0.0",
    "ojs": "libs/oj/v2.0.2/debug",
    "ojL10n": "libs/oj/v2.0.2/ojL10n",
    "ojtranslations": "libs/oj/v2.0.2/resources",
    "knockout-amd-helpers": "libs/knockout/knockout-amd-helpers",
    "text": "libs/require/text",
    "signals": "libs/js-signals/signals",
    "css" : "libs/require-css/css/min",
    "Clarifai":"node_modules/clarifai/dist/browser/clarifai-2.0.12"
  }
  //endinjector
  ,
  // Shim configurations for modules that do not expose AMD
  shim:
  {
    "jquery":
    {
      exports: ["jQuery", "$"]
    },
    
    "underscore":
    {
        exports: ["_", "underscore"]
    }
  }
}
);

require(["ojs/ojcore", "knockout", "jquery", "ojs/ojknockout", "ojs/ojmodule",
  "ojs/ojmoduleanimations","ojs/ojrouter"],
  function (oj, ko, $)
  {
    oj.ModuleBinding.defaults.modelPath = 'viewModels/';
    oj.ModuleBinding.defaults.viewPath = 'text!views/';
    oj.ModuleBinding.defaults.viewSuffix = '.html';
    var router = oj.Router.rootInstance;
    router.configure({
        'home': {label: 'Home', isDefault: true},
        'carListDetails': {label: 'Card List'}
    });
    
    
    function MainViewModel()
    {
      var self = this;
      self.router = router;
      self.titleLabel = ko.observable("Fastcar");
      self.selectedTab = ko.observable(0);
      this.switcher = function(context)
    {
      var params = ko.unwrap(context.valueAccessor()).params;
      // Use the router knowledge of the back button to reverse animation
      if (params.ojRouter.direction === 'back')
      {
        return 'navParent';
      }

      return 'navChild';
    };
        
      self.currentModule = ko.observable('home');
      self.previousModule = ko.observable('home');
      this.currentList = 'home';
      this.currentModule = ko.observable(this.currentList);
       // this.content = ko.observable("");

        this.modulePath = ko.pureComputed(
          function()
          {
            return ('' + self.currentModule());
            
          }
        ); 
//        console.log("path "+ this.modulePath());
 
      self.anim = ko.observable('fadeIn');
      
      self.customAnimation = function(){
           var module = self.currentModule();  
           var preModule = self.previousModule();
          if(preModule === 'home'){
             if (module === 'cardDetails' || module === 'startChat' || module === 'carListDetails' ) {
             return this.customCoverStart;  
               }
               else{
                   return this.customRevealEnd;
               } 
          }
          else{
             if (module === 'cardDetails' || module === 'startChat' || module === 'carListDetails' ) {
             return this.customRevealEnd;  
               }
               else{
                   return this.customCoverStart;
               } 
          }
           
       } 
          
      this.customCoverStart = {
        'canAnimate': function(context) {return true},
        'prepareAnimation': function(context) {return oj.ModuleAnimations['coverStart']['prepareAnimation'](context);},
        'animate': function(context) {return oj.ModuleAnimations['coverStart']['animate'](context);}  
      };
        
      this.customRevealEnd = {
        'canAnimate': function(context) {return true},
        'prepareAnimation': function(context) {return oj.ModuleAnimations['revealEnd']['prepareAnimation'](context);},
        'animate': function(context) {return oj.ModuleAnimations['revealEnd']['animate'](context);}  
      };
      
    }

    $(document).ready(function()
    {   
      oj.Router.defaults['urlAdapter'] = new oj.Router.urlParamAdapter();    
      var vm = new MainViewModel();
      oj.Router.sync().then(
                    function () {
                        ko.applyBindings(vm);
                        //document.getElementById("mainContent")
                        $('#mainContent').show();
                        vm1 = vm;
                    },
                    function (error) {
                        oj.Logger.error('Error in root start: ' + error.message);
                    });    
      

      document.addEventListener("deviceready", onDeviceReady, false);

      function onDeviceReady()
      {
        // Will execute when device is ready, or immediately if the device is already ready.
          console.log(navigator.camera);
          console.log(position);
          
      }
    });
  }
);
