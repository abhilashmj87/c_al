define(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojtabs', 'ojs/ojconveyorbelt','ojs/ojbutton', 'ojs/ojpopup','ojs/ojlistview',
        'ojs/ojmoduleanimations','ojs/ojrouter'],
  function (oj, ko, $) {
    function accContentModel(moduleParams) {
        var self = this;
       
        
        self.backToHomeHandler = function(event, ui){
          console.log(moduleParams);
        //   oj.Router.rootInstance.go('carListDetails');    
         //  moduleParams.content('hello');
           moduleParams.currentModule("home");
           moduleParams.previousModule("home");
      }
        
        self.detailPageHandler = function(event, ui){
          console.log(moduleParams);
        //   oj.Router.rootInstance.go('carListDetails');    
         //  moduleParams.content('hello');
           moduleParams.currentModule("cardDetails");
           moduleParams.previousModule("home");
      }
    }

    return accContentModel ;
});  