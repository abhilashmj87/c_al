define(['ojs/ojcore', 'knockout', 'jquery', "Clarifai", 'ojs/ojknockout', 'ojs/ojtabs',
  'ojs/ojconveyorbelt', 'ojs/ojbutton', 'ojs/ojpopup',
  'ojs/ojmoduleanimations', 'ojs/ojrouter', 'ojs/ojfilmstrip', 'ojs/ojpagingcontrol',
  'ojs/ojchart', 'ojs/ojselectcombobox'
], function(oj, ko, $) {

  function accContentModel(moduleParams) {
    var self = this;

    //console.log(Clarifai);
    var clarifai = new Clarifai.App(
      'Gzl3uulHsX4RVA7fSJsU4VXbJH02jX9oygi9xOyy',
      'NJqCddswux032cRlJXGosIkzWic8c7D3zs1URdc5'
    );
    //console.log(clarifai);
    /*  $.ajax({
          method: "post",
          url: 'https://developer.clarifai.com/v1/token?client_id=Gzl3uulHsX4RVA7fSJsU4VXbJH02jX9oygi9xOyy&client_secret=NJqCddswux032cRlJXGosIkzWic8c7D3zs1URdc5&grant_type=client_credentials',
          success: function(data){
              console.log(data);
          },

          failure: function(a, b, c){
              console.log(a);
          }
      });*/

    self.currentTopic = ko.observable(['']);
    self.topicTemplate = ko.observable('');
    self.topicAnswer = ko.observable('');
    self.topicLevel = ko.observable('');
    self.topicQuestion = ko.observable('');

    var topicNum = 0;
    self.translateTopic = function(){
      $.ajax({
        //url: 'https://apex.oracle.com/pls/apex/calapp/translation/translation?page=1',
        url: 'data.json',
        type: 'GET',
        async: true,
        cache:false,
        beforeSend: function(x)
        {
            if(x && x.overrideMimeType) {
                x.overrideMimeType("application/j-son;charset=UTF-8");
            }
        },
        dataType: "json", 
        success: function(data){
          console.log(topicNum);
          for(items in data.items[topicNum]){
            if(data.items[topicNum].topic == self.topicTemplate()) {
              self.topicQuestion("Do you know what "+ data.items[topicNum].en_word + " is called in korean?");
              self.topicAnswer(data.items[topicNum].ko_word);
              self.topicLevel(data.items[topicNum].learner_level);
              topicNum++ ;
              break;
            }
          }
        },
        error: function(a, b, c){
          console.log(a, b, c);
        }
      });
    };

    self.showTopicWords = function(context, event) {
      if (event.previousValue !== undefined) {
        let val = event.value[0];
        self.topicTemplate(val);
        console.log(self.topicTemplate());
        self.translateTopic();
      }
    };

    self.tab_animate = function(event, ui) {
      // verify that the component firing the event is a component of interest

      if (ui.fromTab.index() > ui.toTab.index()) {
        ui.fromContent.find('.tab-mover').animate({
          left: '-100%'
        });
        ui.toContent.find('.tab-mover').offset({
          top: 0,
          left: 375
        });
        ui.toContent.find('.tab-mover').animate({
          left: 0
        });

      } else {
        ui.fromContent.find('.tab-mover').animate({
          left: '100%'
        });
        ui.toContent.find('.tab-mover').offset({
          top: 0,
          left: -375
        });
        ui.toContent.find('.tab-mover').animate({
          left: 0
        });

      }
    }

    self.pagingModel = null;

    getItemInitialDisplay = function(index) {
      return index < 3 ? '' : 'none';
    };

    getPagingModel = function() {
      if (!self.pagingModel) {
        var filmStrip = $("#filmStrip");
        var pagingModel = filmStrip.ojFilmStrip("getPagingModel");
        self.pagingModel = pagingModel;
      }
      return self.pagingModel;
    };

    self.locQuestion = ko.observable('Do you know?');
    self.locAnswer = ko.observable('');
    self.locResults = ko.observable();

    self.postDetails = function(ko_word, en_word, date) {
      $.ajax({
        url: 'https://apex.oracle.com/pls/apex/calapp/puttranslation/details',
        type: 'POST',
        data: { ko_word, en_word, date },
        dataType: 'json',
        failure: function(a, b, c) {
          console.log(a, b, c);
        }
      });
    }

    self.speakAnswer = function() {
      if (window.SpeechSynthesisUtterance === undefined) {
        console.log("Speech API not supported");
      } else {
        var utterance = new SpeechSynthesisUtterance();
        utterance.lang = 'ko';
        utterance.rate = 1;
        utterance.text = self.locAnswer();
        window.speechSynthesis.speak(utterance);
      }
    }

    self.translateOnceMore = function() {
      self.translateResult(self.locResults());
    }

    var z = 1;
    self.translateResult = function(results) {
      var tempName = results[z].name.split(' ');
      console.log(tempName);
      self.locQuestion('Do you know what ' + tempName[tempName.length - 1] +
        ' is called in korean?');

      /*$.getJSON("https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=ko&dt=t&q=" + encodeURI(tempName[tempName.length - 1]),function(result){
          console.log("hello");
          console.log(result);
      });*/
      var url =
        'https://translate.googleapis.com/translate_a/single?client=gtx&sl=en&tl=ko&dt=t&q=' +
        encodeURI(tempName[tempName.length - 1]);
      console.log(url);
      $.ajax({
        url: url,
        type: 'GET',
        dataType: 'text',
        success: function(data) {
          var temp = data.match(/(?:"[^"]*"|^[^"]*$)/)[0].replace(/"/g, "");
          self.locAnswer(temp);
          console.log("Answer: " + temp);
          self.postDetails(temp, tempName[tempName.length - 1], new Date());
        },
        error: function(a, b, c) {
          alert(a);
          alert(b);
          console.log(b);
        }
      });
      // $.getJSON("http://api.microsofttranslator.com/v2/Http.svc/Translate?text=" + tempName[tempName.length - 1] + "&from=&to=en", function(data){
      // self.locAnswer();
      //  });

      /*$.ajax({
                        url: 'http://api.microsofttranslator.com/V2/Ajax.svc/Translate?appID=Bearer contextawarelearnerapp&text=' + tempName[tempName.length - 1] + '&from=en&to=ko',
                        type: 'GET',
                        dataType: 'jsonp',
                        success: function(data) { alert('hello!'); console.log(data); },
                        error: function() { alert('boo!'); }
                      });

                    function setHeader(xhr) {
                      xhr.setRequestHeader('Authorization', 'Bearer b9h2jTEFoqBuZ4jUQ0/Kal30/gbR1qAGHy8dmG980rs=');
                    }*/

      /*var client = new mstranslator({
        client_id: "c_al"
        , client_secret: "contextawarelearnerapp"
      }, true);

      var params = {
        text: tempName[tempName.length - 1]
        , from: 'en'
        , to: 'ko'
      };

      // Don't worry about access token, it will be auto-generated if needed.
      client.translate(params, function(err, data) {
        console.log(data);
      });*/
      z++;
    }

    self.initMap = function() {
      navigator.geolocation.getCurrentPosition(
        function(position) {
          var myLatLng = { lat: position.coords.latitude, lng: position.coords.longitude };

          // Create a map object and specify the DOM element for display.
          var map = new google.maps.Map(document.getElementById('user_map'), {
            center: myLatLng,
            scrollwheel: false,
            zoom: 14
          });

          // Create a marker and set its position.
          var marker = new google.maps.Marker({
            map: map,
            position: myLatLng,
            title: 'Your Location'
          });

          var infowindow = new google.maps.InfoWindow();
          var service = new google.maps.places.PlacesService(map);

          var request = {
            location: map.getCenter(),
            radius: '500',
            rankBy: google.maps.places.RankBy.PROMINENCE
          };
          service.nearbySearch(request, callback);

          function callback(results, status) {

            console.log(status);
            console.log(results);
            self.locResults(results);
            if (status == google.maps.places.PlacesServiceStatus.OK) {
              console.log("Status OK");
              self.translateResult(results);
            } else {
              self.locAnswer('No points of interest found');
            }
          }
        },
        function(error) {
          alert(error);
          consolel.log(error);
        },
        //Issue 1: Adding timeout to fix geolocation not working on android devices
        { enableHighAccuracy: true }
      );
    }

    self.picAnswer = ko.observable('');
    self.photoName = ko.observable();
    self.scanPhoto = function(data) {
      console.log(data);
      self.photoName(data);
      alert(data);
      clarifai.models.predict(Clarifai.GENERAL_MODEL, data).then(function(response) {
          alert(response);
          self.picAnswer(response.results[0].result.classes[0]);
          alert(response.results[0].result.classes[0]);
        },
        function(err) {
          alert(err);
        }
      );

    }

    self.initCamera = function() {
      navigator.camera.getPicture(self.scanPhoto, self.captureFail, {
        allowEdit: true,
        correctOrientation: true,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        targetHeight: 315,
        targetWidth: 320
      });
    }


    self.goToTab = function(tabNo) {
      //console.log(tabNo);
      switch (tabNo) {
        case 2:
          $('#cal_tabs li.oj-tabs-tab[data-content=tabs-2]').trigger('click');
          break;
        case 3:
          $('#cal_tabs li.oj-tabs-tab[data-content=tabs-3]').trigger('click');
          self.initMap();
          break;
        case 4:
          $('#cal_tabs li.oj-tabs-tab[data-content=tabs-4]').trigger('click');
          break;
        default:
          break;
      }
    }
  }

  return accContentModel;
});
